/*
Merge k sorted linked lists and return it as one sorted list. Analyze and
describe its complexity.
*/
public class mergeKLists {
    public ListNode mergeKLists(ListNode[] lists) {
    	if(lists == null || lists.length == 0) 
    		return null;
    	PriorityQueue<ListNode> minHeap = new PriorityQueue<>(new Comparator<ListNode>() {
    		@Override
    		public int compare(ListNode p, ListNode q) {
    			return Integer.compare(p.val, q.val);
    		}
		});
    	for(ListNode list: lists) {
    		if(list != null)
    		minHeap.offer(list);
    	}
    	ListNode res = new ListNode(0);
    	ListNode cur = res;
    	while(! minHeap.isEmpty()) {
    		cur.next = minHeap.poll();
    		cur = cur.next;
    		if(cur.next != null)
    			minHeap.offer(cur.next);
    	}
    	return res.next;
    }
}
