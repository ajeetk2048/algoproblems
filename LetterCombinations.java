/*
Given a digit string, return all possible letter combinations that the 
number could represent.

A mapping of digit to letters (just like on the telephone buttons) is 
given below.

{0: "0", 1: "1", 2: "abc", 3: "def", 4: "ghi", 5: "jkl", 6: "mno", 
 7: "pqrs", 8: "tuv", 9: "wxyz"}

Input:Digit string "23"
Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
Note:
Although the above answer is in lexicographical order, your answer could 
be in any order you want.
*/
public class LetterCombinations {
	private static final String[] map = new String[]{"0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    public List<String> letterCombinations(String digits) {
        if(digits == null || digits.length() == 0)
            return new ArrayList<String>();
    	return backTrack(digits, 0);
    }
    
    public List<String> backTrack(String digits, int idx) {
    	List<String> res = new ArrayList<>();
    	if(idx == digits.length()) {
    		res.add("");
    		return res;
    	}
    	List<String> temp = backTrack(digits, idx + 1);
    // 	for(char c: map[Character.getNumericValue(digits.charAt(idx))].toCharArray())
        for(char c: map[digits.charAt(idx) - '0'].toCharArray())
    		for(String s: temp) res.add(c + s);
    	return res;
    }
}

// Solution using Queue
/*
public class Solution {
	private static final String[] map = new String[]{"0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    public List<String> letterCombinations(String digits) {
    	LinkedList<String> res = new LinkedList<>();
    	if(digits == null || digits.length() == 0)
    		return res;
    	res.offer("");
    	for(int i = 0; i < digits.length(); i++) {
    		while(res.peek().length() == i) {
    			String s = res.poll();
    			for(char c: map[digits.charAt(i) - '0'].toCharArray()) {
    				res.add(s + c);
    			}
    		}
    	}
    	return res;
    }
}
*/
