import java.util.*;
import java.lang.*;

public class Kmp {
	public static void main(String[] args) {
		if(args.length != 2) {
			System.out.println("Usage: java Kmp  <string> <pattern>");
		}
		else {
			String text = args[0];
			String pattern = args[1];
			
			//Preprocessing: Compute prefix function
			int[] prefixFunc = new int[pattern.length()];
			computePrefixFunc(prefixFunc, pattern);
            // Print prefixFunc values
//			for(int i = 0; i < prefixFunc.length; i++)
//				System.out.println(prefixFunc[i]);
			
			//Matching algorithm
			List<Integer> shiftList = new ArrayList<>();
			int q = 0;
			for(int i = 0 ; i < text.length(); i++) {
				while(q > 0 && pattern.charAt(q) != text.charAt(i))
					q = prefixFunc[q-1];
				if(pattern.charAt(q) == text.charAt(i))
					q += 1;
				if(q == pattern.length()) {
					shiftList.add(i-pattern.length()+1);
					q = prefixFunc[q-1];
				}
			}
            // Print the shifts at which the pattern occurs in given string
            if(shiftList.size() > 0) {
                System.out.print("\"" + pattern + "\" "+ "occurs in \"" + text + "\" with following shifts: ");
    			for(int i = 0; i < shiftList.size(); i++) 
    				System.out.print(shiftList.get(i) + " ");
                System.out.println(" ");
            }
            else
                System.out.println("\"" + pattern + "\" "+ "does not occur in \"" + text + "\"");
		}
	}
	
	static void computePrefixFunc(int[] prefixArr, String pat) {
		prefixArr[0] = 0;
		int k = 0;
		for(int i = 1; i < pat.length(); i++) {
			while(k > 0 && pat.charAt(k) != pat.charAt(i))
				k = prefixArr[k-1];
			if(pat.charAt(k) == pat.charAt(i))
				k += 1;
			prefixArr[i] = k;
		}
	}
}
