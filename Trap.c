/*
Given n non-negative integers representing an elevation map where the width 
of each bar is 1, compute how much water it is able to trap after raining.

For example, 
Given [0,1,0,2,1,0,1,3,2,1,2,1], return 6.
*/
// O(n) space
public class Trap {
    public int trap(int[] height) {
    	if(height == null || height.length == 0) return 0;
    	int len = height.length;
    	int[] lheight = new int[len];
    	int[] rheight = new int[len];
    	int res = 0;
    	lheight[0] = height[0];
    	rheight[len - 1] = height[len - 1];
    	for(int i = 1; i < len; i++)
    		lheight[i] = Math.max(height[i], lheight[i - 1]);
    	for(int i = len - 2; i >= 0; i--) 
    		rheight[i] = Math.max(height[i], rheight[i + 1]);
    	for(int i = 0; i < len; i++) 
    		res += Math.min(lheight[i], rheight[i]) - height[i];
    	return res;
    }
}

// O(1) space
/*
public class Trap {
    public int trap(int[] height) {
    	if(height == null || height.length == 0) return 0;
    	int res = 0, len = height.length;
    	int l = 0, r = len - 1;
    	int lmax = 0, rmax = 0;
    	while(l <= r) {
    		lmax = Math.max(lmax, height[l]);
    		rmax = Math.max(rmax, height[r]);
    		if(lmax < rmax) 
    			res += lmax - height[l++];
    		else 
    			res += rmax - height[r--];
    	}
    	return res;
    }
}
*/
