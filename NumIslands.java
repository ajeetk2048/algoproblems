/*
Given a 2d grid map of '1's (land) and '0's (water), count the number of 
islands. An island is surrounded by water and is formed by connecting adjacent 
lands horizontally or vertically. You may assume all four edges of the grid 
are all surrounded by water.

Example:
11110
11010
11000
00000
Answer 1

11000
11000
00100
00011
Answer 3
*/
public class NumIslands {
	private int[] xdir = {-1, 0, 0, 1};
	private int[] ydir = {0, -1, 1, 0};
	public int numIslands(char[][] grid) {
    	int res = 0;
    	if(grid == null || grid.length == 0 || grid[0].length == 0)
    		return res;
    	for(int r = 0; r < grid.length; r++) {
    		for(int c = 0; c < grid[0].length; c++) {
    			if(grid[r][c] == '1') {
    				res++;
    				dfs(grid, r, c);
    			}
    		}
    	}
    	return res;
    }
    
    public void dfs(char[][] grid, int row, int col) {
    	if(row < 0 || row >= grid.length || 
    	   col < 0 || col >= grid[0].length || grid[row][col] == '0')
    		return;
    	grid[row][col] = '0';
    	for(int i = 0; i < xdir.length; i++) {
    		dfs(grid, row + xdir[i], col + ydir[i]);
    	}
    }
}
