/*
Given a singly linked list, determine if it is a palindrome.

Follow up:
Could you do it in O(n) time and O(1) space?
*/
public class IsPalindrome {
    public boolean isPalindrome(ListNode head) {
    	if(head == null || head.next == null)
    		return true;
    	ListNode slow = head;
    	ListNode fast = head;
    	while(fast.next != null && fast.next.next != null) {
    		slow = slow.next;
    		fast = fast.next.next;
    	}
    	
    	ListNode rev = reverseList(slow.next);
    	while(rev != null) {
    		if(head.val != rev.val) return false;
    		head = head.next;
    		rev = rev.next;
    	}
    	return true;
    }
    
    public ListNode reverseList(ListNode head) {
    	ListNode p = null, c = head, n = null;
    	while(c != null) {
    		n = c.next;
    		c.next = p;
    		p = c;
    		c = n;
    	}
    	return p;
    }
}
