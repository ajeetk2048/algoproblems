/*
Serialization is the process of converting a data structure or object into a
sequence of bits so that it can be stored in a file or memory buffer, or
transmitted across a network connection link to be reconstructed later in the
same or another computer environment.

Design an algorithm to serialize and deserialize a binary search tree. There is
no restriction on how your serialization/deserialization algorithm should work.
You just need to ensure that a binary search tree can be serialized to a string
and this string can be deserialized to the original tree structure.

The encoded string should be as compact as possible.

Note: Do not use class member/global/static variables to store states. Your
serialize and deserialize algorithms should be stateless.

Show Company Tags Show Tags Show Similar Problems
*/
public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
    	StringBuilder sb = new StringBuilder();
    	preOrder(root, sb);
    	return sb.toString();
    }

    public void preOrder(TreeNode node, StringBuilder sb) {
    	if(node == null)
    		sb.append("#,");
    	else {
    		sb.append(node.val + ",");
    		preOrder(node.left, sb);
    		preOrder(node.right, sb);
    	}
    }
    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
    	String[] temp = data.split(",");
    	Queue<String> nodes = new LinkedList<>();
    	nodes.addAll(Arrays.asList(temp));
    	return buildBST(nodes);
    }
    
    public TreeNode buildBST(Queue<String> nodes) {
    	String n = nodes.poll();
    	if(n.equals("#")) return null;
    	TreeNode res = new TreeNode(Integer.parseInt(n));
    	res.left = buildBST(nodes);
    	res.right = buildBST(nodes);
    	return res;
    }
}
