/*
A linked list is given such that each node contains an additional random 
pointer which could point to any node in the list or null.

Return a deep copy of the list.
*/
class RandomListNode {
    int label;
    RandomListNode next, random;
    RandomListNode(int x) { this.label = x; }
};

public class CopyRandomList {
    public RandomListNode copyRandomList(RandomListNode head) {
    	Map<RandomListNode, RandomListNode> map = new HashMap<>();
    	RandomListNode cur = head;
    	while(cur != null) {
    		RandomListNode n = new RandomListNode(cur.label);
    		map.put(cur, n);
    		cur = cur.next;
    	}
    	cur = head;
    	while(cur != null) {
    		map.get(cur).next = map.get(cur.next);
    		map.get(cur).random = map.get(cur.random);
    		cur = cur.next;
    	}
    	return map.get(head);
    }
}
