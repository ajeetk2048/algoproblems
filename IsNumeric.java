/* Given a list of strings, output which of the strings are numeric.
 */

public class IsNumeric {
	public boolean isNumber(String s) {
		// remove any trailing spaces
		s = s.trim();

		boolean isNum = false;
		int i = 0;
		int len = s.length();
		
		// skip any whitespaces at the front
		while(i < len  && s.charAt(i) == ' ')
			i++;
		
		// skip '+' or '-'
		if(i < len && (s.charAt(i) == '+' || s.charAt(i) == '-'))
			i++;
		
		// skip any numeric character
		while(i < len && Character.isDigit(s.charAt(i))) {
			i++;
			isNum = true;
		}
		
		// skip '.' and following digits
		if(i < len && s.charAt(i) == '.') {
			i++;
			while(i < len && Character.isDigit(s.charAt(i))) {
				i++;
				isNum = true;
			}
		}
		
		// skip  'e'/'E' 
		if(isNum && i < len && (s.charAt(i) == 'e' || s.charAt(i) == 'E')) {
			i++;
			isNum = false;  // number can't end with 'e'/'E'
			//skip  '+'/'-'
			if(i < len && (s.charAt(i) == '+' || s.charAt(i) == '-')) 
				i++;
			//skip digits
			while(i < len && Character.isDigit(s.charAt(i))) {
				i++;
				isNum = true;
			}
		}
		return isNum && i == len;
	}
	
	public static void main(String[] args) {
		IsNumeric isNumeric = new IsNumeric();
		for(String s: args) {
			System.out.println(s + "\t\t: " + isNumeric.isNumber(s));
		}
	}
}