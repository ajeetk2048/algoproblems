/*
Given two words (beginWord and endWord), and a dictionary's word list, find 
the length of shortest transformation sequence from beginWord to endWord, 
such that:

Only one letter can be changed at a time.
Each transformed word must exist in the word list. Note that beginWord is not 
a transformed word.

For example,

Given:
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log","cog"]
As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
return its length 5.

Note:
Return 0 if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
You may assume no duplicates in the word list.
You may assume beginWord and endWord are non-empty and are not the same.
*/
public class LadderLength {
    public int ladderLength(String beginWord, String endWord, Set<String> wordSet) {
    	Queue<String> queue = new LinkedList<>();
    	queue.offer(beginWord);
    	int res = 2;
    	while(! queue.isEmpty()) {
    		int size = queue.size();
    		for(int i = 0; i < size; i++) {
    			char[] w = queue.poll().toCharArray();
    			for(int j = 0; j < w.length; j++) {
    				char t = w[j];
    				for(char c = 'a'; c <= 'z'; c++) {
    					if(c != w[j]) {
    						w[j] = c;
    						String s = new String(w);
    						if(wordSet.contains(s)) {
    							if(s.equals(endWord))
    								return res;
    							queue.offer(s);
    							wordSet.remove(s);
    						}
    					}
    				}
    				w[j] = t;
    			}
    		}
    		res++;
    	}
    	return res;
    }
}
