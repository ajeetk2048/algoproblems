/*
Design and implement a data structure for Least Recently Used (LRU) cache. It 
should support the following operations: get and put.

get(key) - Get the value (will always be positive) of the key if the key 
           exists in the cache, otherwise return -1.
put(key, value) - Set or insert the value if the key is not already present. 
                  When the cache reached its capacity, it should invalidate 
                  the least recently used item before inserting a new item.
Example:

    LRUCache cache = new LRUCache( 2:capacity );

cache.put(1, 1);
cache.put(2, 2);
cache.get(1);       // returns 1
cache.put(3, 3);    // evicts key 2
cache.get(2);       // returns -1 (not found)
cache.put(4, 4);    // evicts key 1
cache.get(1);       // returns -1 (not found)
cache.get(3);       // returns 3
cache.get(4);       // returns 4
*/

import java.util.*;
class DLLNode {
    int key;
    int val;
    DLLNode next;
    DLLNode prev;
    public DLLNode() {};
    public DLLNode(int key, int val) {
        this.val = val;
        this.key = key;
    }
}
public class LRUCache {
    int capacity;
    DLLNode head;
    DLLNode tail;
    Map<Integer, DLLNode> cache;
    int currentCount;
    
    public LRUCache(int capacity) {
        this.capacity = capacity;
        head = new DLLNode(); //dummy head
        tail = new DLLNode(); //dummy tail
        head.next = tail;
        head.prev = null;
        tail.prev = head;
        tail.next = null;
        cache = new HashMap<Integer, DLLNode>();
    }
    
    public int get(int key) {
        DLLNode n = cache.get(key);
        if(n == null) return -1;
        moveToHead(n);
        return n.val;
    }
    
    public void put(int key, int value) {
        DLLNode n = cache.get(key);
        if(n == null) {
            n = new DLLNode(key, value);
            setToHead(n);
            cache.put(key, n);
            currentCount++;
            if(currentCount > capacity) {
                cache.remove(tail.prev.key);
                removeNode(tail.prev);
                currentCount--;
            }
        }
        else {
            n.val = value;
            moveToHead(n);
        }
    }
    
    public void moveToHead(DLLNode node) {
        removeNode(node);
        setToHead(node);
    }
    
    public void setToHead(DLLNode node) {
        node.next = head.next;
        head.next = node;
        node.prev = head;
        node.next.prev = node;
    }
    
    public void removeNode(DLLNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }
}
