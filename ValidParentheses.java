/*
Given a string containing just the characters '(', ')', '{', '}', '[' and ']', 
determine if the input string is valid.

The brackets must close in the correct order, "()" and "()[]{}" are all 
valid but "(]" and "([)]" are not.
*/
public class ValidParentheses {
    public boolean isValid(String s) {
    	Stack<Character> stack = new Stack<>();
    	for(int i = 0; i < s.length(); i++) {    
    		Character c = s.charAt(i);
    		if(c == '(' || c == '{' || c == '[')
    			stack.push(c);
    		else if(stack.isEmpty()) return false;
    		else {
    			Character t = stack.pop();
    			if((t == '(' && c == ')') || (t == '{' && c == '}') || (t == '[' && c == ']')) 
    				continue;
    			return false;
    		}
    	}
    	return stack.isEmpty();
    }
}
