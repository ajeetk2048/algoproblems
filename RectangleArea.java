/* Find the total area covered by two rectilinear rectangles in a 2D plane.
 * Each rectangle is defined by its bottom left corner and top right corner 
 * (A, B) : Bottom left corner of rectangle 1
 * (C, D) : Top right corner of rectangle 1
 * (E, F) : Bottom left corner of rectangle 2
 * (G, H) : Top right corner of rectangle 2
 */

import java.util.*;
import java.io.*;
import java.util.regex.*;
public class RectangleArea {
    public static void main(String[] args) throws Exception {
        Scanner s = new Scanner(System.in);
        /*
        int[] arr = new int[8];
        int i = 0;
        System.out.print("Enter coordinate values (8 integers). Ctrl + D when finished:");
        while(s.hasNext())
            arr[i++] = s.nextInt();
        if(i != 8) {
            System.out.println("Incorrect input: 8 integers needed");
            return;
        }
        System.out.println("Total Area covered by the two rectangles is: " + 
                             getArea(arr[0],arr[1],arr[2],arr[3],arr[4],arr[5],arr[6],arr[7]));
        */
        System.out.print("Enter coordinate values (8 integers): ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input_line = br.readLine();
        String[] arr = input_line.split(" ");
        int[] coord = new int[8];
        for(int i = 0; i < arr.length; i++)
            coord[i] = Integer.parseInt(arr[i]);
        System.out.println("Total Area covered by the two rectangles is: " + 
                             getArea(coord[0],coord[1],coord[2],coord[3],coord[4],coord[5],coord[6],coord[7]));
    }

    static int getArea(int A, int B, int C, int D, int E, int F, int G, int H) {
        int xBottom = Math.max(A, E);
        int yBottom = Math.max(B, F);
        int xTop = Math.min(C, G);
        int yTop = Math.min(D, H);

        if(xBottom > xTop || yBottom > yTop)
            return (C - A) * (D - B) + (G - E) * (H - F);
        else
            return (C - A) * (D - B) + (G - E) * (H - F) - (xTop - xBottom) * (yTop - yBottom);
    }
}
