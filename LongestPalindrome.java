/*
Given a string s, find the longest palindromic substring in s. You may assume 
that the maximum length of s is 1000.

Example:

Input: "babad"

Output: "bab"

Note: "aba" is also a valid answer.

Example:

Input: "cbbd"

Output: "bb"
*/
public class LongestPalindrome {
	private int start = 0;
	private int finalLen = 0;
    public String longestPalindrome(String s) {
    	if(s == null || s.length() == 0) 
    		return "";
    	if(s.length() < 2) return s;
    	for(int i = 0; i < s.length() - 1; i++) {
    		expandPalindrome(s, i, i);
    		expandPalindrome(s, i, i + 1);
    	}
    	return s.substring(start, start + finalLen);
    }
    
    public void expandPalindrome(String s, int left, int right) {
    	while(left >=0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
    		left--;
    		right++;
    	}
    	int curLen = right - left - 1;
    	if(curLen > finalLen) {
    		start = left + 1;
    		finalLen = curLen;
    	}
    }
}

// Dynamic programming solution

/*
public class Solution {
    public String longestPalindrome(String s) {
    	int i = 0, j = 0, k = 0;
        int len = s.length();
        int maxlen = 0;  //length of maximum palindromic substring found so far
        int startInd = 0; //start index of result string
        if(len == 0 || len == 1)
            return s;
        // lp[i][j] will be true if s[i...j] is a palindrome
         
        boolean[][] lp = new boolean[len][len];
     
        // All substrings of lenght 1 are palindromes.
        maxlen = 1;
        for(i = 0; i < len; i++)
            lp[i][i] = true;
        maxlen = 1;
     
        // fill up the lp table for all substrings of length 2
        for(i = 0; i < len - 1; i++) {
            if(s.charAt(i) == s.charAt(i+1)) {
            	lp[i][i+1] = true;
            	startInd = i;
            	maxlen = 2;
            }
        }
     
        // fill up the lp table for string length 3 or more using 
        // the following relation
        // lp[i][j] = lp[i+1][j-1] && (s.charAt(i) == s.charAt(j)
        // start looking for palindromic substrings of length 3
        // and go upto length of the whole string.
        for(k = 3; k <= len; k++) {
        	for(i = 0; i < len-k+1; i++) {
        		// Endpoint of substring of length k starting from index i
        		j = i + k -1;
        		lp[i][j] = lp[i+1][j-1] && (s.charAt(i) == s.charAt(j));
        		if(lp[i][j]) {
        			startInd = i;
        			maxlen = k;
        		}
        	}
        }
    return s.substring(startInd , startInd + maxlen);
 
    }
}
*/
