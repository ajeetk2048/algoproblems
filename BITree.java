/* Implementation of Binary Index Tree */

class BITree {
  public static void main(String[] args) {
    int[] inputArr = {21,1,91,3,2,13,4,5,6,17,9,6};
    int[] BIT = createBIT(inputArr);
    for(int i = 0; i < BIT.length;  i++)
      System.out.print(BIT[i] + " ");
    System.out.println();
    for(int i = 0; i < inputArr.length;  i++)
      System.out.print(getSum(BIT, i+1) + " ");
  }

  static int[] createBIT(int[] arr) {
    int len = arr.length;
    int[] retArr = new int[len +1];  
    for(int i = 0; i <= len; i++)
      retArr[i] = 0;
    for(int i = 0; i < len; i++) {
      updateBIT(retArr, i + 1, arr[i]);
    }
    return retArr;
  }

  static void updateBIT(int[] bit, int index, int val) {
    while(index < bit.length) {
      bit[index] += val;
      index += (index & (-index));
    }
  }

  static int getSum(int[] bit, int index) {
    int ret = 0;
    while(index > 0) {
      ret += bit[index];
      index = (index - (index & (-index)));
    }
    return ret;
  }

}
