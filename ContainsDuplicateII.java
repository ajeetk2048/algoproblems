/* Java program to find nearby duplicate in an integer array.
 * input: integer array nums
 *        max-distance k
 * output: True, if nums array has duplicates at most k-distance apart
 *         False, otherwise.
 */
import java.util.*;
class ContainsDuplicateII {
    static boolean containsNearbyDuplicate(int[] nums, int k) {
        HashMap<Integer, Integer> hmap = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
            if(hmap.containsKey(nums[i]) && (i - hmap.get(nums[i])) <= k)
                return true;
            hmap.put(nums[i], i);
        }
        return false;
    }

    /* Driver function */
    public static void main(String[] args) {
        int k = 0, n = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("<Integer> (max-distance k): ");
        k = sc.nextInt();
        System.out.print("<Integer> (sizeof array n): ");
        n = sc.nextInt();
        System.out.print("<int array>: ");

        int[] nums = new int[n];

        for(int i = 0; i < n; i++)
            nums[i] = sc.nextInt();

        System.out.println(containsNearbyDuplicate(nums, k) ? "True" : "False");
    }
}
