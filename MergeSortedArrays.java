/* Java program to merge two sorted arrays.
 * input: Array1 of integers in non-increasing order, #elements 'm' in Array1
 *        Array2 of integers in non-increasing order, #elements 'n' in Array2
 *        Additional requirement: (sizeof(Array1) >= m + n)
 * output: Array1 contains all elements in non-increasing order.
 */

import java.util.*;
class MergeSortedArrays {
    static void merge(int[] arr1, int m, int[] arr2, int n) {
        int idx = m + n -1;
        m--;
        n--;
        while(m >= 0 && n >= 0) {
            if(arr1[m] >= arr2[n])
                arr1[idx--] = arr1[m--];
            else
                arr1[idx--] = arr2[n--];
        }
        while(n >= 0)
            arr1[idx--] = arr2[n--];
    }

    /* Driver function */
    public static void main(String[] args) {
        int m = 0, n = 0, i = 0;

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter m and n: ");
        m = sc.nextInt();
        n = sc.nextInt();

        int[] arr1 = new int[m+n];
        int[] arr2 = new int[n];

        System.out.print("Enter Array1 elements: ");
        for(i = 0; i < m; i++)
            arr1[i] = sc.nextInt();
        System.out.print("Enter Array2 elements: ");
        for(i = 0; i < n; i++)
            arr2[i] = sc.nextInt();

        merge(arr1, m, arr2, n);

        System.out.print("The merged array is: ");
        for(i = 0; i < m + n; i++)
            System.out.print(arr1[i] + " ");
    }
}
