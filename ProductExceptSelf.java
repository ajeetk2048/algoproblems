/*
Given an array of n integers where n > 1, nums, return an array output such 
that output[i] is equal to the product of all the elements of nums except 
nums[i].

Solve it without division and in O(n).

For example, given [1,2,3,4], return [24,12,8,6].

Follow up:
Could you solve it with constant space complexity? (Note: The output array 
does not count as extra space for the purpose of space complexity analysis.)
*/
//O(1) Solution
public class ProductExceptSelf {
    public int[] productExceptSelf(int[] nums) {
    	int[] res = new int[nums.length];
    	res[0] = 1;
    	for(int i = 1; i < nums.length; i++)
    		res[i] = res[i - 1] * nums[i - 1];
    	int rightSideProduct = 1;
    	for(int i = nums.length - 1; i >= 0; i--) {
    		res[i] = res[i] * rightSideProduct;
    		rightSideProduct *= nums[i];
    	}
    	return res;
    }
}

//O(n) space solution
/*
public class ProductExceptSelf {
  public int[] productExceptSelf(int[] nums) {
  	int len = nums.length;
  	int[] leftSideProducts = new int[len];
  	int[] rightSideProducts = new int[len];
  	int[] retArr = new int[len];
  	leftSideProducts[0] = 1;
  	rightSideProducts[len - 1] = 1;
  	for(int i = 1; i < len; i++)
  		leftSideProducts[i] = leftSideProducts[i-1] * nums[i-1];
  	for(int i = len - 2; i >= 0 ; i--)
  		rightSideProducts[i] = rightSideProducts[i + 1] * nums[i+1];
  	for(int i = 0; i < len; i++)
  		retArr[i] = leftSideProducts[i] * rightSideProducts[i];
  	return retArr;
  }
}
*/
