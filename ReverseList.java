/*
Reverse a singly linked list.
*/
public class ReverseList {
    public ListNode reverseList(ListNode head) {
    	ListNode p = null;
    	ListNode c = head;
    	ListNode n = null;
    	while(c != null) {
    		n = c.next;
    		c.next = p;
    		p = c;
    		c = n;
    	}
    	return p;
    }
}
