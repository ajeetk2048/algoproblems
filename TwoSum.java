/*
Given an array of integers, return indices of the two numbers such that they 
add up to a specific target.

You may assume that each input would have exactly one solution.
Example: 
Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
*/

import java.util.*;
// O(n) solution using hashmap
public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
        	if(map.containsKey(target - nums[i]))
        		return new int[]{i, map.get(target-nums[i])};
        	map.put(nums[i], i);
        }
        return new int[]{-1,-1};
    }
}

// O(nlogn) solution using sorting
/*
class Pair {
	int index;
	int val;
	public Pair() {};
	public Pair(int i, int v) {
		index = i;
		val = v;
	}
}
public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        Pair[] pnums = new Pair[nums.length];
        for(int i = 0; i < nums.length; i++) 
        	pnums[i] = new Pair(i, nums[i]);
        Arrays.sort(pnums, new Comparator<Pair>(){
        	@Override
        	public int compare(Pair p, Pair q) {
        		return Integer.compare(p.val, q.val);
        	}
        });
        for(int i = 0, j = pnums.length - 1; i < j; ) {
        	int n = pnums[i].val + pnums[j].val;
        	if(n == target) return new int[]{pnums[i].index, pnums[j].index};
        	else if(n < target) i++;
        	else j--;
        }
        return new int[]{-1, -1};
    }
}
*/

// O(n^2) solution brute force
/*
public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        int i = 0, j = 0, diff = 0;
        int[] result = new int[]{-1, -1};
        for(i = 0; i < nums.length; i++) {
            diff = target - nums[i];
            for(j = i + 1; j < nums.length; j++) {
                if(diff == nums[j]) {
                    result[0] = i + 1;
                    result[1] = j + 1;
                    return result;
                }
            }
        }
        return result;
    }
}
*/
