/*
Serialization is the process of converting a data structure or object into a 
sequence of bits so that it can be stored in a file or memory buffer, or 
transmitted across a network connection link to be reconstructed later in the
same or another computer environment.

Design an algorithm to serialize and deserialize a binary tree. There is no 
restriction on how your serialization/deserialization algorithm should work. 
You just need to ensure that a binary tree can be serialized to a string and 
this string can be deserialized to the original tree structure.

For example, you may serialize the following tree
    1
   / \
  2   3
     / \
    4   5

as "[1,2,3,null,null,4,5]", just the same as how LeetCode OJ serializes a 
binary tree. You do not necessarily need to follow this format, so please 
be creative and come up with different approaches yourself.
*/

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */

public class SerialDeserialBtree {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
    	StringBuilder sb = new StringBuilder();
    	preOrder(root, sb);
    	return sb.toString();
    }
    
    public void preOrder(TreeNode node, StringBuilder sb) {
    	if(node == null) 
    		sb.append("#,");
    	else {
    		sb.append(node.val + ",");
    		preOrder(node.left, sb);
    		preOrder(node.right, sb);
    	}
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
    	String[] parts = data.split(",");
    	Queue<String> nodes = new LinkedList<>();
    	nodes.addAll(Arrays.asList(parts));
    	return buildTree(nodes);
    }
    
    public TreeNode buildTree(Queue<String> nodes) {
    	String node = nodes.poll();
    	if(node.equals("#")) return null;
    	TreeNode temp = new TreeNode(Integer.parseInt(node));
    	temp.left = buildTree(nodes);
    	temp.right = buildTree(nodes);
    	return temp;
    }
}
