/* Given a non-empty string s and a dictionary wordDict containing a list of
non-empty words, determine if s can be segmented into a space-separated
sequence of one or more dictionary words. You may assume the dictionary does
not contain duplicate words.

For example, given s = "leetcode", dict = ["leet", "code"].

Return true because "leetcode" can be segmented as "leet code".  
*/

// //O(stringLength ** 2)
public class WordBreak {
    public boolean wordBreak(String s, Set<String> wordDict) {
    	int len = s.length();
    	boolean[] dp = new boolean[len + 1];
    	dp[0] = true;
    	for(int i = 0; i <= len; i++) {
    		for(int j = 0; j < i; j++) {
    			if(dp[j] && wordDict.contains(s.substring(j, i))){
    				dp[i] = true;
    				break;
    			}
    		}
    	}
    	return dp[len];
    }
}

//O(stringLength * wordDictSize)
/*
public class WordBreak {
    public boolean wordBreak(String s, Set<String> wordDict) {
    	int len = s.length();
    	boolean[] wb_dp = new boolean[len + 1];
    	wb_dp[0] = true;
    	
    	for(int i = 0; i < len; i++) {
    		if(wb_dp[i]) {
	    		for(String w: wordDict) {
	    			int wLen = w.length();
	    			if(i + wLen <= len) {
		    			if(!wb_dp[i + wLen] && s.substring(i, i + wLen).equals(w))
		    				wb_dp[i + wLen] = true;
	    			}
	    		}
    		}
    	}
    	
    	return wb_dp[len];
    }
}
*/
