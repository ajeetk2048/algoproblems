public class EditDistance {
    public static void main(String[] args) {
        if(args.length != 2) {
            System.out.println("Usage: java EditDistance  <string1> <string2>");
            System.out.println("Returns: Minimum number of steps required to convert string1 to string2");
            System.out.println("The operation allowed are: insert a character, delete a character, replace a character");
        }
        else {
            String word1 = args[0];
            String word2 = args[1];
            System.out.println("Min #steps requried to convert " + word1 + " to " + word2 + " is: " + minDistance(word1, word2));
        }
    }

    //EditDistance implementaion using dynamic programming
    static int minDistance(String word1, String word2) {
        int row = word1.length();
        int col = word2.length();
        if(row == 0)
            return col;
        if(col == 0)
            return row;
        int[][] dist = new int[row+1][col+1];
        int i = 0;
        for(i = 0; i <= row; i++)
            dist[i][0] = i;
        for(i = 0; i <= col; i++)
            dist[0][i] = i;
        for(i = 1; i <= row; i++) {
            for(int j = 1; j <= col; j++) {
                // td holds the distance between the char in word1 at i-1 and char in word2 at j-1
                int td = 0;
                if(word1.charAt(i - 1) != word2.charAt(j -1))
                    td = 1;
                dist[i][j] = Math.min(Math.min(dist[i-1][j-1] + td, 1 + dist[i-1][j]), 1 + dist[i][j-1]);
            }
        }
        return dist[row][col];
    }
}
