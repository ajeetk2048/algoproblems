/* Java program to find all prime number upto a given non-negative integer
 * Input: A non-negative integer(n) in commandline
 * Output: A boolean array of size n+1 indicating primality of each number
 */
import java.util.*;
class AllPrimes {
    public static void main(String[] args) {
        if(args.length != 1) {
            System.out.println("Usage: java AllPrimes <Non-negative integer>");
            return;
        }
        int num = 0;
        try {
            num = Integer.parseInt(args[0]);
        }catch(NumberFormatException e) {
            System.out.println("Wrong Input: Integer expected, given:" + e.getMessage().split(":")[1]);
            return;
        }

        boolean[] isPrime = findAllPrimes(num);

        //Print the boolean array indicating primality
        //System.out.println("Number\tisPrime");
        //for(int i = 1; i <= num; i++) {
        //    System.out.println(i + "\t" + isPrime[i]);
        //}
    }

    static boolean[] findAllPrimes(int num) {
        boolean[] primes = new boolean[num + 1];
        Arrays.fill(primes, true);
        primes[0] = false;
        primes[1] = false;

        int r = (int)Math.sqrt(num);

        for(int i = 2; i <= r; i++) {
            if(primes[i]) {
                for(int j = i*i; j <= num; j+=i)
                    primes[j] = false;
            }
        }
        return primes;
    }
}
