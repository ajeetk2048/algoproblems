/*
Write a program to find the node at which the intersection of two singly linked lists begins.


For example, the following two linked lists:

A:          a1 → a2
                   ↘
                     c1 → c2 → c3
                   ↗            
B:     b1 → b2 → b3
begin to intersect at node c1.


Notes:

If the two linked lists have no intersection at all, return null.
The linked lists must retain their original structure after the function returns.
You may assume there are no cycles anywhere in the entire linked structure.
Your code should preferably run in O(n) time and use only O(1) memory.
*/

// Soution utilizing cycle
public class GetIntersectionNode {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
    	if(headA == null || headB == null) return null;
    	ListNode temp = headA;
    	while(temp.next != null) temp = temp.next;
    	temp.next = headA;
    	ListNode res = detectCycle(headB);
    	temp.next = null;
    	return res;
    }
    
    public ListNode detectCycle(ListNode head) {
        if(head == null || head.next == null) return null;
    	ListNode slow = head;
    	ListNode fast = head;
    	while(fast != null && fast.next != null) {
    		slow = slow.next;
    		fast = fast.next.next;
    		if(slow == fast) break;
    	}
    	if(slow != fast) return null;
    	slow = head;
    	while(slow != fast) {
    		slow = slow.next;
    		fast = fast.next;
    	}
    	return slow;
    }
}

// Solution utilizing difference in length
/*
public class  GetIntersectionNode{
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
    	if(headA == null || headB == null) return null;
    	int lenA = 0, lenB = 0;
    	ListNode travA = headA, travB = headB;
    	while(travA != null) {
    		travA = travA.next;
    		lenA++;
    	}
    	while(travB != null) {
    		travB = travB.next;
    		lenB++;
    	}
    	travA = lenA > lenB ? headA : headB; //travA points to longer list
    	travB = lenA > lenB ? headB : headA; //travB points to smaller list
    	
    	int diff = Math.abs(lenA - lenB);
    	while(diff-- > 0) travA = travA.next;
    	while(travA != travB) {
    		travA = travA.next;
    		travB = travB.next;
    	}
    	return travA;
    }
}
*/

// Solution without knowing lengths
/*
public class getIntersectionNode {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
    	if(headA == null || headB == null) return null;
    	ListNode travA = headA;
    	ListNode travB = headB;
    	while(travA != travB) {
    		travA = travA == null ? headB : travA.next; 
    		travB = travB == null ? headA : travB.next;
    	}
    	return travA;
    }
}
*/
