/* Java program to test primality of an Integer.
 * Input: An integer in commmandline
 * Output: Prints whether the input given is Prime or not
 */
class Prime {
    public static void main(String[] args) {
        if(args.length != 1) {
            System.out.println("Usage: java Prime <Non-negative integer>");
            return;
        }
        int num = 0;
        try {
            num = Integer.parseInt(args[0]);
        }catch(NumberFormatException e) {
            System.out.println("Wrong Input: Integer expected, given:" + e.getMessage().split(":")[1]);
            return;
        }
        if(isPrime(num))
            System.out.println(num + " is prime.");
        else
            System.out.println(num + " is not prime.");
        }

    static boolean isPrime(int num) {
        if(num <= 1)
            return false;
        if(num == 2)
            return true;
        if(num % 2 == 0)
            return false;
        int r = Math.sqrt(num);
        for(int i = 3; i <= r; i+=2) {
            if(num % i == 0)
                return false;
        }
        return true;
    }
}
