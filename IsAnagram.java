/*
Given two strings s and t, write a function to determine if t is an anagram of s.

For example,
s = "anagram", t = "nagaram", return true.
s = "rat", t = "car", return false.

Note:
You may assume the string contains only lowercase alphabets.

Follow up:
What if the inputs contain unicode characters? How would you adapt your solution to such case?
*/
public class IsAnagram {
    public boolean isAnagram(String s, String t) {
    	if(s.length() != t.length()) return false;
    	int[] charCount = new int[26];
    	for(int i = 0; i < s.length(); i++) {
    		charCount[s.charAt(i) -'a']++;
    		charCount[t.charAt(i) - 'a']--;
    	}
    	for(int c: charCount) 
    		if(c != 0) return false;
    	return true;
    }
}

