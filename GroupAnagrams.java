/*
Given an array of strings, group anagrams together.

For example, given: ["eat", "tea", "tan", "ate", "nat", "bat"], 
Return:

[
  ["ate", "eat","tea"],
  ["nat","tan"],
  ["bat"]
]
Note: All inputs will be in lower-case.
*/
public class groupAnagrams {
    public List<List<String>> groupAnagrams(String[] strs) {
    	Map<String, List<String>> map = new HashMap<>();
    	for(String str: strs) {
    		char[] temp = str.toCharArray();
    		Arrays.sort(temp);
    		String s = new String(temp);
    		if(! map.containsKey(s))
    			map.put(s, new ArrayList<String>());
    		map.get(s).add(str);
    	}
    	return new ArrayList<>(map.values());
    }
}

// similar approach with different map
/*
public class GroupAnagrams {
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> res = new ArrayList<List<String>>();
        HashMap<String, Integer> map = new HashMap<>();
        int i = 0;
        for(String str: strs) {
        	char[] temp = str.toCharArray();
        	Arrays.sort(temp);
        	String s = new String(temp);
        	if(! map.containsKey(s)) {
        		res.add(i, new ArrayList<>());
        		map.put(s, i++);
        	}
    		res.get(map.get(s)).add(str);
        }
        return res;
    }
}
*/
