/* Java program to find length of longest increasing subsequence in an integer array.
 * input: integer array nums
 * output: length of longest increasing subsequence in nums 
 */
import java.util.*;
class LIS {
  static int longestIncSubseq(int[] nums) {
    if(nums.length == 0)
      return 0;
    int retval = 0;
    int[] lis = new int[nums.length];
    lis[0] = 1;
    for(int i = 0; i < nums.length; i++) {
      lis[i] = 1;
      for(int j = 0; j < i; j++) {
        if(nums[j] < nums[i] && lis[i] < lis[j] + 1)
	  lis[i] = lis[j] + 1;
      }
    }
    for(int i = 0; i < nums.length; i++) {
      if(retval < lis[i])
        retval = lis[i];
    }
    return retval;
  }

  public static void main(String[] args) {
    int n = 0;
    Scanner sc = new Scanner(System.in);
    System.out.print("<Integer> (sizeof array n): ");
    n = sc.nextInt();
    System.out.print("<int array>: ");

    int[] nums = new int[n];

    for(int i = 0; i < n; i++)
      nums[i] = sc.nextInt();
    System.out.println("lenght of longest increasing subsequence is: " + longestIncSubseq(nums));
  }
}
