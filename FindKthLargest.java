/*
Find the kth largest element in an unsorted array. Note that it is the kth
largest element in the sorted order, not the kth distinct element.

For example,
Given [3,2,1,5,6,4] and k = 2, return 5.

Note: 
You may assume k is always valid, 1 ≤ k ≤ array's length.
*/

public class FindKthLargest {
    public int findKthLargest(int[] nums, int k) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for(int n: nums) {
        	minHeap.offer(n);
        	if(minHeap.size() > k)
        		minHeap.poll();
        }
        return minHeap.peek();
    }
}

// Using quickselect
/*
public class Solution {
    public int findKthLargest(int[] nums, int k) {
    	return nums[findKthSmallest(nums, nums.length - k + 1, 0, nums.length - 1)];
    }
    
    public int findKthSmallest(int[] nums, int k, int start, int end) {
    	int pivot = nums[end];
    	int i = start;
    	int j = end;
    	while(i < j) {
    		if(nums[i] > pivot) {
    			swap(nums, i, --j);
    		}
    		else 
    			i++;
    	}
    	swap(nums, i, end);
    	int count = i - start + 1;
    	if(count == k) return i;
    	else if(count > k) return findKthSmallest(nums, k, start, i - 1);
    	else return findKthSmallest(nums, k - count, i + 1, end);
    }
    
    public void swap(int[] nums, int i, int j) {
    	int temp = nums[i];
    	nums[i] = nums[j];
    	nums[j] = temp;
    }
}
*/
